import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Person } from '../../Person';

@Component({
  selector: 'ap-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input()
  contacts: Person[];

  @Output()
  contactSelected = new EventEmitter<Person>();

  select(contact: Person) {
    this.contactSelected.emit(contact);
  }

}
