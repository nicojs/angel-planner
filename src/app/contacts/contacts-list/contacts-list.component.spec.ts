import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ContactsListComponent } from './contacts-list.component';
import { Person, Gender } from '../../Person';
import { CapitalizePipe } from '../../general/capitalize/capitalize.pipe';
import { GeneralModule } from '../../general/general.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('Contact list', () => {

  let fixture: ComponentFixture<ContactsListComponent>;
  let nativeElement: HTMLElement;
  let sut: ContactsListComponent;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        ContactsListComponent
      ],
      imports: [
        GeneralModule
      ]
    });
    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ContactsListComponent);
    nativeElement = fixture.nativeElement;
    sut = fixture.componentInstance;
  });

  it('should show 2 contacts', () => {
    sut.contacts = [
      new Person(undefined, 'Foo', 42, Gender.Female),
      new Person(undefined, 'Bar', 34, Gender.Male)
    ];
    fixture.detectChanges();
    const rowCount = nativeElement.querySelectorAll('tbody tr').length;
    expect(rowCount).toBe(2);
  });

  it('should capitalize contact names', () => {
    sut.contacts = [
      new Person(undefined, 'foo', 42, Gender.Other)
    ];
    fixture.detectChanges();
    const nameTableCell = nativeElement.querySelector<HTMLElement>('tbody tr:first-child td:first-child');
    const fooName = nameTableCell.textContent;
    expect(fooName).toBe('Foo');
  });
});
