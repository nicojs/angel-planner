import { Component, OnInit, AfterContentInit, DoCheck, Injector, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person, Gender, PersonJson } from '../../Person';
import { map, tap, flatMap, filter } from 'rxjs/operators'
import { ContactService, SaveAction } from '../contact.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ap-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit, AfterContentInit, DoCheck {

  allContacts: Person[];

  constructor(
    private contactService: ContactService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  selectedContact = new Person(undefined, '', 0, Gender.Other);

  ngOnInit() {
    this.contactService
      .getAll()
      .subscribe(contacts => this.allContacts = contacts,
        error => console.error(error),
        () => console.log('Observable closed'));

    // If the route has a contact id, use that to edit a contact
    this.route.params.pipe(

      filter(params => 'id' in params),
      map(params => parseInt(params.id, 10)),
      flatMap(id => this.contactService.get(id))

    ).subscribe(contact => this.selectedContact = contact);
  }

  ngAfterContentInit(): void {
    console.log('contacts component ngAfterContentInit');
  }

  ngDoCheck(): void {
    console.log('contacts component ngDoCheck');
  }

  selectContact(contact: Person) {
    this.router.navigate(['/contacts', contact.id]);
  }

  get isEdit() {
    return typeof this.selectedContact.id !== 'undefined';
  }

  saveContact(contact: Person) {
    this.contactService
      .save(contact)
      .subscribe(contactAction => {
        if (contactAction.action === SaveAction.Add) {
          this.allContacts.push(contactAction.person);
        } else {
          this.allContacts = this.allContacts.map(c => c.id === contactAction.person.id ? contactAction.person  : c);
        }
      });
    this.selectedContact = new Person(undefined, '', 0, Gender.Other);
  }

}
