import { HttpClient } from "@angular/common/http";
import { PersonJson, Person } from "../Person";
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

export interface PersonAction {
  person: Person;
  action: SaveAction;
}

export enum SaveAction {
  'Update',
  'Add'
};

@Injectable()
export class ContactService {

  private readonly url = 'http://localhost:3000/contacts';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<PersonJson[]>(this.url)
      .pipe(
        map(people => people.map(Person.fromDto)),
    );
  }

  get(id: number): Observable<Person> {
    return this.http.get<PersonJson>(`${this.url}/${id}`)
      .pipe(
        map(Person.fromDto)
      );
  }

  save(contact: Person): Observable<PersonAction> {
    return this.putOrPost(contact)
      .pipe(map(result => ({ action: result.action, person: Person.fromDto(result.personJson) })));
  }

  private putOrPost(contact: Person): Observable<{ personJson: PersonJson, action: SaveAction }> {
    const toPersonJsonAction = (personJson: PersonJson, action: SaveAction) => ({ personJson, action });
    if (contact.id) {
      return this.http.put<PersonJson>(`${this.url}/${contact.id}`, contact)
        .pipe(
          map(personJson => ({ personJson, action: SaveAction.Update }))
        );
    } else {
      return this.http.post<PersonJson>(this.url, contact)
        .pipe(
          map(personJson => ({ personJson, action: SaveAction.Add }))
        );
    }
  }
}
