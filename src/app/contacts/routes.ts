import { Route } from "@angular/router";
import { ContactsComponent } from "./contacts/contacts.component";

export const routes: Route[] = [
  { path: ':id', component: ContactsComponent },
  { path: '**', component: ContactsComponent }
];
