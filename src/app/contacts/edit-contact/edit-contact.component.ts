import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Person, Gender } from '../../Person';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { map, tap, flatMap } from 'rxjs/operators';

@Component({
  selector: 'ap-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent {

  @Output()
  contactSaved = new EventEmitter<Person>();

  constructor() { }

  @Input()
  contact: Person;

  addPerson(form: NgForm) {
    if (form.valid) {
      this.contactSaved.emit(this.contact);
      form.reset();
    }
  }

  get isEdit() {
    return typeof this.contact.id !== 'undefined';
  }
}
