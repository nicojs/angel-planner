import { NgModule } from "@angular/core";
import { ContactsComponent } from "./contacts/contacts.component";
import { ContactService } from "./contact.service";
import { EditContactComponent } from "./edit-contact/edit-contact.component";
import { ContactsListComponent } from "./contacts-list/contacts-list.component";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { GeneralModule } from "../general/general.module";
import { routes } from "./routes";
import { CommonModule } from "@angular/common";

@NgModule({
  declarations: [
    EditContactComponent,
    ContactsComponent,
    ContactsListComponent
  ],
  providers: [
    ContactService
  ],
  imports: [
    FormsModule,
    CommonModule,
    GeneralModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    ContactsComponent
  ]
})
export class ContactsModule {
}
