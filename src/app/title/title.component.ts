import { Component, Input } from '@angular/core';


@Component({
  selector: 'ap-title',
  template: `<h1 class="col-md-12">Welcome to {{name}}</h1>`
})
export class TitleComponent {

  @Input()
  name: string;
}
