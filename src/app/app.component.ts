import { Component } from '@angular/core';
import { Person, Gender } from './Person';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';
import { Shift } from './Shift';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css'
  ]
})
export class AppComponent {
  title = 'Angel planner';

  constructor() {
    setInterval(() => this.title = new Date().getTime().toString(), 2000);
  }

  shifts: Shift[] = [];
}
