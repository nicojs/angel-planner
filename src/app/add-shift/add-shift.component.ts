import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Shift } from '../Shift';

@Component({
  selector: 'ap-add-shift',
  templateUrl: './add-shift.component.html',
  styleUrls: ['./add-shift.component.css']
})
export class AddShiftComponent {

  @Output()
  shiftAdded = new EventEmitter<Shift>();

  newShiftForm = new FormGroup({
    fromDate: new FormControl(undefined, Validators.required),
    toDate: new FormControl(undefined, Validators.required)
  });

  constructor() {
    this.newShiftForm.valueChanges.subscribe(value => {
      console.log(value);
    });
  }

  addShift() {
    if (this.newShiftForm.valid) {
      this.shiftAdded.emit(new Shift(this.newShiftForm.value));
      this.newShiftForm.reset();
    }
  }
}
