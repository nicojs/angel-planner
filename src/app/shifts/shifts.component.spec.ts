import { TestBed, ComponentFixture, getTestBed } from '@angular/core/testing';
import { ShiftsComponent } from './shifts.component';
import { ShiftListComponent } from '../shift-list/shift-list.component';
import { AddShiftComponent } from '../add-shift/add-shift.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ShiftDto } from '../Shift';

describe('Shifts component', () => {

  let fixture: ComponentFixture<ShiftsComponent>;
  let sut: ShiftsComponent;
  let element: HTMLElement;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [
        ShiftsComponent,
        ShiftListComponent,
        AddShiftComponent
      ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule
      ]
    });

    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ShiftsComponent);
    sut = fixture.componentInstance;
    element = fixture.nativeElement;
    const injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should fetch shifts from server', async () => {
    const shiftsDto: ShiftDto[] = [
      {
        fromDate: '2010-1-1',
        toDate: '2010-1-2'
      }
    ];
    const mockedRequest = httpMock.expectOne('http://localhost:3000/shifts');
    mockedRequest.flush(shiftsDto);
    fixture.detectChanges();
    await fixture.whenStable();
    const shiftsCount = element.querySelector('#shiftsCount').textContent;
    expect(shiftsCount).toBe('1');
  });
});
