import { Component, OnInit } from '@angular/core';
import { Shift, ShiftDto } from '../Shift';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ap-shifts',
  templateUrl: './shifts.component.html',
  styleUrls: ['./shifts.component.css']
})
export class ShiftsComponent implements OnInit {

  allShifts: Shift[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<ShiftDto[]>('http://localhost:3000/shifts')
      .pipe(
        map(shifts => shifts.map(s => new Shift(s)))
      )
      .subscribe(shifts => this.allShifts = shifts);
  }

  addShift(shift: Shift) {
    this.allShifts.push(shift);
  }
}
