import { Component, OnInit, Input } from '@angular/core';
import { Shift } from '../Shift';

@Component({
  selector: 'ap-shift-list',
  templateUrl: './shift-list.component.html'
})
export class ShiftListComponent implements OnInit {

  @Input()
  shifts: Shift[];

  constructor() { }

  ngOnInit() {
  }

}
