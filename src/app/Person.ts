export enum Gender {
  Female,
  Male,
  Other
}


export class Person {

  constructor(
    public id: number | undefined,
    public name: string,
    public age: number,
    public gender: Gender) {
  }

  getGenderName() {
    return Gender[this.gender]
  }

  static fromDto(dto: PersonJson): Person {
    return new Person(dto.id, dto.name, dto.age, dto.gender);
  }
}

export interface PersonJson {
  id?: number;
  gender: number;
  name: string;
  age: number;
}

