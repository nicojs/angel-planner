import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '../../Person';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(value: Person | null) {
    if (value) {
      return `${value.name.charAt(0).toUpperCase()}${value.name.substr(1)}`;
    } else {
      return '';
    }
  }
}
