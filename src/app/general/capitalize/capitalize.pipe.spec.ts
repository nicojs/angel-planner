import { CapitalizePipe } from './capitalize.pipe';
import { Person, Gender } from '../../Person';

describe('Capitalize', () => {

  let sut: CapitalizePipe;

  beforeEach(() => {
    sut = new CapitalizePipe();
  });

  describe('in happy flow', () => {
    beforeEach(() => {
      console.log('starting an happy flow');
    });

    it('should capitalize the first character of the persons name', () => {
      // Arrange
      const input = new Person(undefined, 'foobar', 42, Gender.Other);

      // Act
      const output = sut.transform(input);

      // Assert
      expect(output).toBe('Foobar');
    });

    it('should return the name if it was already capitalized', () => {
      const input = 'Foobar';
      const output = sut.transform(new Person(undefined, input, 42, Gender.Other));
      expect(output).toBe(input);
      expect(new Person(undefined, input, 42, Gender.Other)).toEqual(new Person(undefined, input, 42, Gender.Other));
    });
  });

  describe('unhappy flow', () => {

    beforeEach(() => {
      console.log('starting an unhappy flow');
    });

    it('should transform to an empty string if input was `null`', () => {
      const output = sut.transform(null);
      expect(output).toBe('');
    });

  });


});
