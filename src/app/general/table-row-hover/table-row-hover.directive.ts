import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[apTableRowHover]'
})
export class TableRowHoverDirective {

  private element: HTMLElement;

  constructor(elementRef: ElementRef) {
    this.element = elementRef.nativeElement;
  }

  @Input('apTableRowHover')
  selectable: any;

  @HostListener('mouseleave')
  public mouseLeave() {
    this.element.classList.remove('table-primary');
    this.selectable.isSelected = false;
  }

  @HostListener('mouseenter')
  public mouseEnter() {
    this.element.classList.add('table-primary');
    this.selectable.isSelected = true;
  }
}
