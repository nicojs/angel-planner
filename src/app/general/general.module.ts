import { NgModule } from '@angular/core';
import { CapitalizePipe } from './capitalize/capitalize.pipe';
import { TableRowHoverDirective } from './table-row-hover/table-row-hover.directive';
import { InvalidClassDirective } from './invalid-class/invalid-class.directive';

@NgModule({
  declarations: [
    CapitalizePipe,
    TableRowHoverDirective,
    InvalidClassDirective
  ],
  exports: [
    CapitalizePipe,
    TableRowHoverDirective,
    InvalidClassDirective
  ]
})
export class GeneralModule {
}
