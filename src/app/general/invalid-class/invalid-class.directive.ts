import { Directive, ElementRef, Host, AfterContentChecked } from "@angular/core";
import { NgModel } from "@angular/forms";

@Directive({
  selector: 'input[ngModel]'
})
export class InvalidClassDirective implements AfterContentChecked {

  private element: HTMLElement;

  constructor(elementRef: ElementRef,
    @Host() private ngModel: NgModel) {
    this.element = elementRef.nativeElement;
  }

  ngAfterContentChecked(): void {
    if (this.ngModel.dirty && this.ngModel.invalid) {
      this.element.classList.add('is-invalid');
    } else {
      this.element.classList.remove('is-invalid');
    }
  }
}
