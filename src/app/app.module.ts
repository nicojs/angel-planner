import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { registerLocaleData, APP_BASE_HREF } from '@angular/common';
import localeNL from '@angular/common/locales/nl';
import { GeneralModule } from './general/general.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TitleComponent } from './title/title.component';
import { ShiftsComponent } from './shifts/shifts.component';
import { AddShiftComponent } from './add-shift/add-shift.component';
import { ShiftListComponent } from './shift-list/shift-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHttpInterceptor } from './ErrorHttpInterceptor';
import { RouterModule } from '@angular/router';

registerLocaleData(localeNL);

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    ShiftsComponent,
    AddShiftComponent,
    ShiftListComponent
  ],
  imports: [
    BrowserModule,
    GeneralModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'shifts', component: ShiftsComponent },
      { path: 'contacts', loadChildren: './contacts/contacts.module#ContactsModule'},
      { path: '**', redirectTo: 'contacts' }
    ])
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'nl-NL'
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
