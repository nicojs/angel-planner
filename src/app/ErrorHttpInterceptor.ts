import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";

export class ErrorHttpInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newReq = req.clone({
      headers: req.headers
        .set('Authorization', 'Basic BASE64ENCODED')
        .set('X-Token', 'secret')
    });

    return next.handle(newReq).pipe(
      catchError(error => {
        console.error('General catch for error', error);
        return of(null);
      })
    );
  }
}
