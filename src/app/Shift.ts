
export class Shift {
  public fromDate: Date | undefined;
  public toDate: Date | undefined;

  constructor(jsonRepresentation: ShiftDto) {
    if (jsonRepresentation) {
      this.fromDate = new Date(jsonRepresentation.fromDate);
      this.toDate = new Date(jsonRepresentation.toDate);
    }
  };
}

export interface ShiftDto {
  fromDate: string;
  toDate: string;
}
