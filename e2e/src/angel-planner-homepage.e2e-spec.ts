import { HomePage } from "./HomePage.po";

describe('Angel planner homepage', () => {

  let sut: HomePage;

  beforeEach(async () => {
    sut = new HomePage();
    await sut.navigateTo();
  });

  it('should have the angel planner title', () => {
    expect(sut.title)
      .toBe('Welcome to Angel planner!');
  });

  describe('add person', () => {

    it('should give an error if the name has less then 3 characters', async () => {
      return sut.setName('fo')
        .then(() => sut.errorFeedback)
        .then(errorMsg => {
          expect(sut.errorFeedback)
            .toBe('Name should be a minimal of 3 characters');
        });
    });
  });
});

