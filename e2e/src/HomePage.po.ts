import { browser, element, by } from "protractor";


export class HomePage {

  navigateTo() {
    return browser.get('/');
  }

  drag() {
    browser
      .dragAndDrop(element(by.css('h1')), element(by.css('h2'));
  }

  get title() {
    return element(by.css('h1')).getText();
  }

  setName(name: string) {
    const nameInput = element(by.css('#nameInput'));
    return nameInput.sendKeys(name);
  }

  get errorFeedback() {
    return element(by.css('.invalid-feedback')).getText();
  }
}
